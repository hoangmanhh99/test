//
//  ViewController.swift
//  test
//
//  Created by User on 23/02/2021.
//

import UIKit
import UserNotifications
import CoreLocation

class NotiViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, CLLocationManagerDelegate {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var slideImageView: UIPageControl!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    @IBOutlet weak var preNextStackView: UIStackView!
    @IBOutlet weak var preButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
        
    let pics = ["paperplane", "map", "person"]
    
    let manager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        contentView.layer.cornerRadius = 30
        openButton.layer.cornerRadius = 10
        
        preButton.layer.cornerRadius = 10
        nextButton.layer.cornerRadius = 10
        
        slideImageView.numberOfPages = pics.count
        slideImageView.currentPage = 0
        
        contentLabel.text = "Berichtgeving"
        textLabel.text = "Schakel berichgeving in zodat we u op de hoogte kunnen houden van uw bestellingen en promoties. "
        
        openButton.isHidden = false
        preNextStackView.isHidden = true
    }

    // MARK: Actions
    @IBAction func openLocationView(_ sender: Any) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) {(granted, error) in
            if granted {
                print("User gave permissions for local notifications")
            }
        }
        contentLabel.text = "Locatievoorziening"
        if imageCollectionView.contentOffset.x < (self.imageCollectionView.contentSize.width - (self.view.frame.width)) {
            imageCollectionView.contentOffset.x += self.view.bounds.width
        }
        
        openButton.isHidden = true
        preNextStackView.isHidden = false
    }
    
    @IBAction func preButton(_ sender: Any) {
        nextButton.setTitle("Volgende >", for: .normal)
        if imageCollectionView.contentOffset.x > 0 {
            self.imageCollectionView.contentOffset.x -= self.view.bounds.width
        }
        
        if slideImageView.currentPage == 0 {
            openButton.isHidden = false
            preNextStackView.isHidden = true
        }
    }
    
    @IBAction func nextButton(_ sender: Any) {
        nextButton.setTitle("Voltooien", for: .normal)
        if imageCollectionView.contentOffset.x < (self.imageCollectionView.contentSize.width - (self.view.frame.width)) {
            imageCollectionView.contentOffset.x += self.view.bounds.width
        }
        
        if slideImageView.currentPage == 2 {
            contentLabel.text = "Maak een account aan"
        }
        
        if nextButton.titleLabel?.text == "Voltooien" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let mainVC = storyboard.instantiateViewController(withIdentifier: "MainAppController") as? MainAppController else { return }
            mainVC.headerTitle = "DEmO"
            self.navigationController?.pushViewController(mainVC, animated: true)
            print("Hello")
        }
        
        manager.requestWhenInUseAuthorization()
        manager.delegate = self
        switch manager.authorizationStatus {
        case .restricted, .denied:
            print("No access")
        case .authorizedAlways, .authorizedWhenInUse:
            print("Access")
        case .notDetermined:
            break
        @unknown default:
            break
        }
        
    }
    
    // MARK: CollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = imageCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        if let vc = cell.viewWithTag(111) as? UIImageView {
            vc.image = UIImage(named: pics[indexPath.row])
        } else if let ab = cell.viewWithTag(222) as? UIPageControl{
            ab.currentPage = indexPath.row
        }
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        slideImageView.currentPage = Int(imageCollectionView.contentOffset.x / CGFloat(375))
    }
}

// MARK: Custom CollectionView
extension NotiViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
}

